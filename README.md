# lego4scrum

Un support pour les enseignants à utiliser quasiment sans formation préalable : [Le support](https://gitlab.com/tclavier/lego4scrum/builds/artifacts/master/file/notes_pour_le_facilitateur.pdf?job=build_pdf)

Traduis et adapté de http://www.lego4crum.com

Notez que Howard Gardner a longuement écrit sur les intelligences multiples et la capacité de l’être humain à démultiplier ses capacités en combinant plusieurs d’entre elles en même temps. On notera tout particulièrement l’intelligence corporelle-kinesthésique qu’il décrit comme la capacité d’utiliser le contrôle de ses mouvements. Cette dernière a d’ailleurs été reprise par de nombreux chercheurs comme base de la “kinesthetic learning” ou “tactile learning”.
